(function ($, window, document, undefined) {

	$(document).ready(function($) {

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});

		// Disabled Link
		$('a.disabled').click( function() {
			return false;
		});

		// Home/Customer Stories Carousel
		$('.customer-stories-carousel').slick({
			arrows: false,
			dots: true,
	  		slidesToShow: 1,
			autoplay: true,
			autoplaySpeed: 6000,
		});


		// Nav Overlay Toggle
		$('.nav-trigger').click(function(){
			
			$('nav').fadeToggle(200);
			$('body').toggleClass('nav-overlay-open');

			return false;
		});


		// YouTube Video Overlay Show
		$('.video-trigger').click(function(){
			
			var videoID = $(this).data('video-id');

			$('#video-overlay .video-frame .content').html('<iframe width="1920" height="1080" allowfullscreen frameborder="0" allowTransparency="true" src="https://www.youtube.com/embed/' + videoID + '?autoplay=1& modestbranding=1&rel=0"></iframe>');
			$('.video-frame').fitVids();
			$('#video-overlay').fadeIn(200);
			$('body').addClass('video-overlay-open');

			return false;
		});

		// Vimeo Video Overlay Show
		$('.vimeo-video-trigger').click(function(){
			
			var videoID = $(this).data('video-id');
			$('#video-overlay .video-frame .content').html('<iframe width="1920" height="1080" src="https://player.vimeo.com/video/' + videoID + '?autoplay=1&controls=1&byline=0&playsinline=1&quality=1080p" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
			$('.video-frame').fitVids();
			$('#video-overlay').fadeIn(200);
			$('body').addClass('video-overlay-open');

			return false;
		});



		// Video Overlay Hide
		$('.video-close-btn').click(function(){

			$('#video-overlay').fadeOut(200);
			$('body').removeClass('video-overlay-open');
			$('#video-overlay .video-frame .content').empty();

			return false;
		});


		// Demo Overlay Show
		$('.demo-trigger').click(function(){
			
			$('#demo-overlay').fadeIn(200);
			$('body').addClass('demo-overlay-open');

			return false;
		});


		// Demo Overlay Hide
		$('.demo-close-btn').click(function(){

			$('#demo-overlay').fadeOut(200);
			$('body').removeClass('demo-overlay-open');

			return false;
		});




		// Contact Overlay Show
		$('.contact-trigger').click(function(){
			
			$('#contact-overlay').fadeIn(200);
			$('body').addClass('contact-overlay-open');

			return false;
		});


		// Contact Overlay Hide
		$('.contact-close-btn').click(function(){

			$('#contact-overlay').fadeOut(200);
			$('body').removeClass('contact-overlay-open');

			return false;
		});



		$('.featured-media .video').fitVids();


		// History/Timeline
		$('.timeline').slick({
			arrows: false,
			dots: true,
	  		slidesToShow: 1,
			autoplay: true,
			autoplaySpeed: 6000,
		});

		$('section.hero .cta .btn[href^="#"').smoothScroll({offset: -120});




		// Company/Leadership Carousel 1
		$('.carousel-1').slick({
			arrows: false,
			dots: true,
	  		slidesToShow: 1,
	  		speed: 1000,
			autoplay: true,
			autoplaySpeed: 5000,
		});



		// Company/Leadership Carousel 2
		$('.carousel-2').slick({
			arrows: false,
			dots: true,
	  		slidesToShow: 1,
	  		speed: 1000,
			autoplay: true,
			autoplaySpeed: 7000,
		});


		// Company/Leadership Carousel 3
		$('.carousel-3').slick({
			arrows: false,
			dots: true,
	  		slidesToShow: 1,
	  		speed: 1000,
			autoplay: true,
			autoplaySpeed: 11000,
		});


		$('.case-study-toggle').on('click', function(){

			var target = $(this).data('case-study');

			$(target).toggle();

			$(this).text(function(i, text){
				return text === "Read More +" ? "Hide —" : "Read More +";
			});

			return false;
		});


		// LEADERSHIP GRID

		$('.team-member').on('click', function(){
			$('.team-member').removeClass('active');
			$(this).addClass('active');
			$('.team-grid .insert').remove();			

			if ($(window).width() <= 767) {
				var offset = 200;
				if($(this).hasClass('even')) {
					var next_row_end = $(this);
				} else {
					var next_row_end = $(this).nextAll('.even:first');
				}
			} else {
				var offset = 400;
				if($(this).hasClass('third')) {
					var next_row_end = $(this);
				} else {
					var next_row_end = $(this).nextAll('.third:first');
				}
			}

			console.log(next_row_end);

			let target = $(this).data('target');
			$('.bios-container').find(`[data-id='${target}']`).clone(true).insertAfter(next_row_end);


			

		});
		
	});

	$(window).resize(function(){
		$('.team-member').removeClass('active');
		$('.team-grid .insert').remove();
	});

	$(document).on('click', '.insert .close', function() {
		$('.team-member').removeClass('active');
		$('.team-grid .insert').remove();

		return false
	});

	window.almComplete = function(alm){
		$('.video-trigger').click(function(){
			
			var videoID = $(this).data('video-id');

			$('#video-overlay .video-frame .content').html('<iframe width="1920" height="1080" allowfullscreen frameborder="0" allowTransparency="true" src="https://www.youtube.com/embed/' + videoID + '?autoplay=1& modestbranding=1&rel=0"></iframe>');
			$('.video-frame').fitVids();
			$('#video-overlay').fadeIn(200);
			$('body').addClass('video-overlay-open');

			return false;
		});


		// Vimeo Video Overlay Show
		$('.vimeo-video-trigger').click(function(){
			
			var videoID = $(this).data('video-id');
			$('#video-overlay .video-frame .content').html('<iframe width="1920" height="1080" src="https://player.vimeo.com/video/' + videoID + '?autoplay=1&controls=1&byline=0&playsinline=1&quality=1080p" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
			$('.video-frame').fitVids();
			$('#video-overlay').fadeIn(200);
			$('body').addClass('video-overlay-open');

			return false;
		});
	};


})(jQuery, window, document);