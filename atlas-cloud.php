<?php

/*

	Template Name: Atlas Cloud

*/

get_header(); ?>

	<?php get_template_part('partials/atlas-cloud/hero'); ?>

	<?php get_template_part('partials/atlas-cloud/about'); ?>

	<?php get_template_part('partials/atlas-cloud/challenges-solutions'); ?>

	<?php get_template_part('partials/atlas-cloud/architecture'); ?>

	<?php get_template_part('partials/atlas-cloud/acms'); ?>

	<?php get_template_part('partials/atlas-cloud/approach'); ?>

<?php get_footer(); ?>