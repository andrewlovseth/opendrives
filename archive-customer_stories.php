<?php get_header(); ?>

	<section class="hero">
		<div class="content">

			<div class="photo">
				<img src="<?php $image = get_field('customer_stories_hero_image', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="info">
				<div class="wrapper">
					<div class="headline">
						<h1><?php the_field('customer_stories_headline', 'options'); ?></h1>
					</div>

					<div class="copy p1">
						<p><?php the_field('customer_stories_dek', 'options'); ?></p>
					</div>
				</div>
			</div>

		</div>
	</section>

	<section class="case-studies">
		<div class="wrapper">
			
			<div class="case-studies-grid">
				
				<?php
					$args = array(
						'post_type' => 'customer_stories',
						'posts_per_page' => 50
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<div class="case-study">
						<div class="photo">
							<a href="<?php the_permalink(); ?>">
								<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>

						<div class="headline">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						</div>

						<div class="copy p4">
							<?php the_field('teaser_description'); ?>
						</div>
					</div>

				<?php endwhile; endif; wp_reset_postdata(); ?>

			</div>

		</div>
	</section>

<?php get_footer(); ?>