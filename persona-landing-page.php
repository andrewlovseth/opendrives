<?php

/*

	Template Name: Persona Landing Page

*/

get_header(); ?>

	<?php get_template_part('partials/content/hero'); ?>

	<?php get_template_part('partials/persona-landing-page/features'); ?>

	<?php get_template_part('partials/persona-landing-page/links'); ?>

	<?php get_template_part('partials/persona-landing-page/message'); ?>

<?php get_footer(); ?>