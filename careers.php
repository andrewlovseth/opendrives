<?php

/*

	Template Name: Careers

*/

get_header(); ?>

	<?php get_template_part('partials/content/hero'); ?>

	<section class="careers-info">
		<div class="wrapper">
			
			<div class="headline">
				<h2><?php the_field('careers_headline'); ?></h2>
			</div>

			<div class="copy p2 extended">
				<?php the_field('careers_copy'); ?>
			</div>

		</div>
	</section>

	<section class="job-listings">
		<div class="wrapper">
			
			<div class="embed">
				<?php the_field('careers_embed'); ?>
			</div>

			<?php // get_template_part('partials/careers/cmo'); ?>

		</div>
	</section>

<?php get_footer(); ?>