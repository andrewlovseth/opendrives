<section class="hero">
	<div class="content">

		<div class="photo">
			<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>

		<div class="info">
			<div class="wrapper">
				<div class="headline">
					<h1>
						<?php if(get_field('hero_headline')): ?>
							<?php the_field('hero_headline'); ?>
						<?php else: ?>
							<?php the_title(); ?>
						<?php endif; ?>
					</h1>
				</div>

				<?php if(get_field('hero_dek')): ?>
					<div class="copy p1">
						<p><?php the_field('hero_dek'); ?></p>
					</div>
				<?php endif; ?>

				<?php if(get_field('hero_sub_headline')): ?>
					<div class="sub-headline">
						<h2><?php the_field('hero_sub_headline'); ?></h2>
					</div>
				<?php endif; ?>

				<?php
					$link = get_field('hero_cta');
					if( $link ):
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				?>

					<div class="cta">
						<a href="<?php echo esc_url($link_url); ?>" class="btn" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
					</div>

				<?php endif; ?>

			</div>
		</div>

	</div>
</section>