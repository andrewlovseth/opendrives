<section id="demo-overlay">
	<div class="overlay">
		<div class="overlay-wrapper">

			<div class="info">
				<div class="close">
					<a href="#" class="demo-close-btn close-btn"></a>
				</div>

				<div class="demo-content">
					<div class="content">

						<div class="headline">
							<h3><?php the_field('demo_form_headline', 'options'); ?></h3>
						</div>

						<div class="copy p3">
							<p><?php the_field('demo_form_dek', 'options'); ?></p>
						</div>

						<div class="form-wrapper">
							<?php
								$shortcode = get_field('demo_form_shortcode', 'options');
								echo do_shortcode($shortcode);
							?>
						</div>
					    
					</div>				    	
				</div>
			</div>
			
		</div>
	</div>
</section>