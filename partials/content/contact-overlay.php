<section id="contact-overlay">
	<div class="overlay">
		<div class="overlay-wrapper">

			<div class="info">
				<div class="close">
					<a href="#" class="contact-close-btn close-btn"></a>
				</div>

				<div class="contact-content">
					<div class="content">

						<div class="headline">
							<h3><?php the_field('contact_form_headline', 'options'); ?></h3>
						</div>

						<div class="form-wrapper">
							<?php
								$shortcode = get_field('contact_form_shortcode', 'options');
								echo do_shortcode($shortcode);
							?>
						</div>
					    
					</div>				    	
				</div>
			</div>
			
		</div>
	</div>
</section>