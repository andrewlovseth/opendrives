<?php
    $difference = get_field('difference');
    $image = $difference['photo'];
    $headline = $difference['headline'];
    $copy = $difference['copy'];
?>

<section class="difference">
    <div class="wrapper">

        <div class="two-col-grid">
            <div class="photo">
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>

            <div class="info">
                <div class="headline">
                    <h3><?php echo $headline; ?></h3>
                </div>

                <div class="copy p4 extended">
                    <?php echo $copy; ?>
                </div>
            </div>
        </div>

    </div>
</section>