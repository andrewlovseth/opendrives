<?php 

$solutions_header = get_field('solutions_header');


?>

<section class="solutions-header">
    <div class="wrapper">

        <div class="section-header">
            <div class="headline">
                <h2><?php echo $solutions_header['headline']; ?></h2>
            </div>

            <div class="copy p2 dek align-center">
                <?php echo $solutions_header['dek']; ?>
            </div>            
        </div>

    </div>
</section>

