<?php 

$sections = array('challenge', 'solution');

?>

<section class="comparison">
    <div class="wrapper">

        <div class="two-col-grid">
            <?php foreach($sections as $section): ?>

                <?php
                    $fields = get_field($section);
                    $image = $fields['photo'];
                    $headline = $fields['headline'];
                    $copy = $fields['copy'];
                ?>

                <div class="<?php echo $section; ?> col">
                    <div class="photo">
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </div>

                    <div class="info">
                        <div class="headline">
                            <h4><?php echo $headline; ?></h4>
                        </div>

                        <div class="copy p4 extended">
                            <?php echo $copy; ?>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>    
        </div>

    </div>
</section>

