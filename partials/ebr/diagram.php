<section class="diagram">
    <div class="wrapper">
    
        <div class="headline">
            <h3><?php the_field('diagram_headline'); ?></h3>
        </div>
    
        <div class="image">
            <img src="<?php $image = get_field('diagram'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        </div>
        
    </div>
</section>