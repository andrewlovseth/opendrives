<section class="hero">
	<div class="content">

		<div class="photo">
			<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>

		<div class="info">
			<div class="wrapper">
				<div class="headline">
                    <?php if(get_field('hero_icon')): ?>
                        <div class="icon">
                        <img src="<?php $image = get_field('hero_icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />                        
                        </div>
                    <?php endif; ?>

					<h1>
						<?php if(get_field('hero_headline')): ?>
							<?php the_field('hero_headline'); ?>
						<?php else: ?>
							<?php the_title(); ?>
						<?php endif; ?>
					</h1>
				</div>

				<?php if(get_field('hero_sub_headline')): ?>
					<div class="sub-headline">
						<h2><?php the_field('hero_sub_headline'); ?></h2>
					</div>
				<?php endif; ?>

				<?php if(get_field('hero_dek')): ?>
					<div class="copy p1">
						<p><?php the_field('hero_dek'); ?></p>
					</div>
				<?php endif; ?>
			</div>
		</div>

	</div>
</section>