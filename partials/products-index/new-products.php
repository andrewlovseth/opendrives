<section class="products-list new-products">
	<div class="wrapper">

		<div class="section-header">
			<div class="headline">
				<h2><?php the_field('products_headline'); ?></h2>
			</div>

			<div class="copy p3">
				<p><?php the_field('products_dek'); ?></p>
			</div>
		</div>

		<div class="products-grid">
			<?php if(have_rows('products')): while(have_rows('products')): the_row(); ?>

				<?php 
					$link = get_sub_field('link');
					if( $link ) {
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					}
				 ?>

				<div class="product">
					<div class="photo">
						<a href="<?php echo $link_url; ?>">
							<span class="product-name">
								<?php the_sub_field('headline'); ?>
							</span>

							<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</a>
					</div>

					<div class="info">
						<div class="info-wrapper">
							<div class="copy p4">
								<?php the_sub_field('dek'); ?>
							</div>

							<?php if( $link ): ?>
							 	<div class="cta">
							 		<a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
							 	</div>
							<?php endif; ?>								
						</div>
					</div>
				</div>

			<?php endwhile; endif; ?>				
		</div>
		
	</div>
</section>