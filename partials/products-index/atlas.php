<?php

$link = get_field('atlas_link');
$link_url = $link['url'];
$link_title = $link['title'];
$link_target = $link['target'] ? $link['target'] : '_self';

$atlas_cloud_link = get_field('atlas_cloud_link');
$atlas_cloud_link_url = $atlas_cloud_link['url'];
$atlas_cloud_link_title = $atlas_cloud_link['title'];
$atlas_cloud_link_target = $atlas_cloud_link['target'] ? $link['target'] : '_self';


?>

<section class="atlas">
	<div class="wrapper">

		<div class="section-header headline">
			<h2><?php the_field('atlas_section_header'); ?></h2>
		</div>

		<div class="photo">
			<?php 
				if( $link ): 
			?>

				<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
					<img src="<?php $image = get_field('atlas_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>

			<?php endif; ?>
		</div>

		<div class="info">
			<div class="info-wrapper">
				<div class="headline">
					<h3><?php the_field('atlas_headline'); ?></h3>
				</div>

				<div class="copy p4">
					<p><?php the_field('atlas_dek'); ?></p>
				</div>

				<?php if( $link ): ?>
				 	<div class="cta">
				 		<a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
				 	</div>
				<?php endif; ?>

				<div class="cloud">
					<div class="headline">
						<h3><?php the_field('atlas_cloud_headline'); ?></h3>
					</div>

					<div class="copy p4">
						<p><?php the_field('atlas_cloud_dek'); ?></p>
					</div>

					<?php if( $atlas_cloud_link ): ?>
						<div class="cta">
							<a class="btn" href="<?php echo esc_url($atlas_cloud_link_url); ?>" target="<?php echo esc_attr($atlas_cloud_link_target); ?>"><?php echo esc_html($atlas_cloud_link_title); ?></a>
						</div>
					<?php endif; ?>	
				</div>




			</div>
		</div>

	</div>
</section>