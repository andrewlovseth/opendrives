<?php

    $cta = get_field('cta');
    $headline = $cta['headline'];
    $deck = $cta['deck'];
    $link = $cta['link'];

?>

<section class="cta">
	<div class="wrapper">
			
		<div class="headline">
			<h2><?php echo $headline ?></h2>
		</div>

		<div class="copy p1">
			<p><?php echo $deck ?></p>
		</div>

		<?php 
			if( $link ): 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
		 ?>

		 	<div class="cta">
		 		<a class="blue-btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
		 	</div>

		<?php endif; ?>

	</div>
</section>