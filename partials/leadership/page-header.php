<?php

    $page_header = get_field('page_header');
    $headline = $page_header['headline'];
    $copy = $page_header['copy'];

?>

<section class="page-header">
	<div class="wrapper">
			
		<div class="headline">
			<h1><?php echo $headline ?></h1>
		</div>

		<div class="copy p1">
			<p><?php echo $copy ?></p>
		</div>

	</div>
</section>