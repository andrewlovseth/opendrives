<section class="team">
    <div class="team-grid">
    
        <?php if(have_rows('team')): $count = 1; while(have_rows('team')) : the_row(); ?>
            <?php if( get_row_layout() == 'team_member' ): ?>

                <div data-target="<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>" class="team-member team-member-<?php echo $count; ?><?php if($count % 2 == 0): ?> even<?php endif; ?><?php if($count % 3 == 0): ?> third<?php endif; ?>">
                    <div class="photo">
                        <img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </div>

                    <div class="info">
                        <div class="headline name">
                            <h3><?php the_sub_field('name'); ?></h3>
                        </div>

                        <div class="sub-headline position">
                            <h4><?php the_sub_field('position'); ?></h4>
                        </div>

                        <div class="bio"></div>
                    </div>                        
                </div>

            <?php endif; ?>
        <?php $count++; endwhile; endif; ?>

    </div>

    <div class="bios-container">
        <?php if(have_rows('team')): while(have_rows('team')) : the_row(); ?>
            <?php if( get_row_layout() == 'team_member' ): ?>

                <?php
                    $name = get_sub_field('name');
                    $position = get_sub_field('position');
                    $linkedin = get_sub_field('linkedin');
                    $bio = get_sub_field('biography');
                    $badges = get_sub_field('badges');
                ?>

				<div class="insert" data-id="<?php echo sanitize_title_with_dashes($name); ?>">
					<div class="insert-wrapper">
						<a href="#" class="close">Close</a>
						<div class="bio">
                            <div class="headline">
                                <h4><?php echo $name; ?></h4>
                                <h5><?php echo $position; ?></h5>
                            </div>
                            <div class="copy p3 extended">
                                <?php echo $bio; ?>
                            </div>

                            <div class="meta">
                                <?php if( $badges ): ?>
                                    <div class="badges">
                                        <?php foreach( $badges as $badge): ?>
                                            <div class="badge">
                                                <?php echo wp_get_attachment_image($badge['ID'], 'full'); ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>

                                <?php if($linkedin): ?>
                                    <div class="linkedin">
                                        <a href="<?php echo $linkedin; ?>" target="blank">LinkedIn</a>
                                    </div>
                                <?php endif; ?>
                            </div>
						</div>					
					</div>
				</div>

            <?php endif; ?>
        <?php endwhile; endif; ?>
    </div>
</section>