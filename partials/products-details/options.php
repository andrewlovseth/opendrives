<section class="options">
	<div class="wrapper">

		<div class="headline">
			<h2><?php the_field('options_headline'); ?></h2>
		</div>

		<?php $copy = get_field('options_copy'); if($copy): ?>

			<div class="copy p4">
				<div class="col col-1">
					<?php echo $copy['column_1']; ?>
				</div>

				<div class="col col-2">
					<?php echo $copy['column_2']; ?>
				</div>
			</div>
		
		<?php endif; ?>

		<?php if(have_rows('options_comparison')): ?>

			<div class="options grid three-col">

				<?php while(have_rows('options_comparison')): the_row(); ?>

					<?php $product = get_sub_field('product'); if( $product ): ?>

						<a href="<?php echo esc_url( get_page_link( $product->ID ) ); ?>" class="option <?php echo sanitize_title_with_dashes(get_field('hero_product_name', $product->ID)); ?><?php if(get_sub_field('active')): ?> active<?php endif; ?>">

							<?php $option = get_field('product_options', $product->ID); if($option): ?>

								<div class="series-name">
									<h4><?php echo $option['series']; ?></h4>
								</div>

								<div class="compute-module-name">
									<h5><?php echo $option['compute_module_name']; ?></h5>
								</div>

							<?php endif; ?>

							<?php if(have_rows('product_options', $product->ID)): while(have_rows('product_options', $product->ID)): the_row(); ?>

								<?php if(have_rows('module_options')): 
									$product_options = get_field('product_options', $product->ID);
									$modules = $product_options['module_options'];
									$count = count($modules);
								?>
									<div class="module-options options-<?php echo $count; ?>">

										<?php $index = 1; while(have_rows('module_options')): the_row(); ?>

											<div class="module module-<?php echo $index; ?>">
												<p><?php the_sub_field('module'); ?></p>
											</div>

										<?php $index++; endwhile; ?>

									</div>
								<?php endif; ?>

							<?php endwhile; endif; ?>

						</a>
	
					<?php endif; ?>

				<?php endwhile; ?>

			</div>

		<?php endif; ?>

	</div>
</section>