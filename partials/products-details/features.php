<section class="features">
	<div class="wrapper">
		
		<div class="photo">
			<img src="<?php $image = get_field('features_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />			
		</div>	

		<div class="info">
			<div class="info-wrapper">

				<div class="headline">
					<h3><?php the_field('features_headline'); ?></h3>
				</div>

				<div class="sub-headline">
					<h4><?php the_field('features_sub_headline'); ?></h4>
				</div>

				<div class="copy p2">
					<?php the_field('features_copy'); ?>
				</div>				

			</div>
		</div>

	</div>
</section>