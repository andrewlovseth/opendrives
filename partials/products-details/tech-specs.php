<section class="tech-specs <?php echo $post->post_name; ?>">
	<div class="wrapper">

        <div class="tables">
            <?php if(have_rows('tech_specs')): while(have_rows('tech_specs')) : the_row(); ?>
                
                <?php if( get_row_layout() == 'section_header' ): ?>
                    <div class="section-header headline">
                        <h4><?php the_sub_field('headline'); ?></h4>
                    </div>
                <?php endif; ?>

                <?php if( get_row_layout() == 'table' ): ?>
                    <div class="spec-table<?php if(get_sub_field('full_width') == TRUE): ?> full-width<?php endif; ?>">
                        <?php the_sub_field('html'); ?>
                    </div>
                <?php endif; ?>

            <?php endwhile; endif; ?>
        </div>

	</div>
</section>