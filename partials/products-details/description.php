<section class="product-description">
	<div class="wrapper">
		
		<div class="info">
			<div class="copy p4">
				<?php the_field('description'); ?>
			</div>

			<?php if(have_rows('description_links')): ?>
				<div class="links">

					<?php while(have_rows('description_links')): the_row(); ?>
	 
						 <?php 
							$link = get_sub_field('link');
							if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						 ?>

						 	<div class="cta">
						 		<a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						 	</div>

						<?php endif; ?>

					<?php endwhile; ?>
					
				</div>
			<?php endif; ?>
		</div>

		<div class="photo">
			<img src="<?php $image = get_field('description_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>



	</div>
</section>