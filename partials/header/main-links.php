<div class="main-links">
    <?php if(have_rows('main_nav', 'options')): while(have_rows('main_nav', 'options')) : the_row(); ?>

        <?php if( get_row_layout() == 'basic_link' ): ?>

            <?php 
                $link = get_sub_field('link');
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?> 

                <div class="link">
                    <a class="blue-underline" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                </div>

            <?php endif; ?>

        <?php endif; ?>


        <?php if( get_row_layout() == 'dropdown' ): ?>
            <?php
                $disabled = get_sub_field('disable_top_level');
                $link = get_sub_field('top_level_link');
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>

                <div class="link dropdown-link">
                    <a class="blue-underline<?php if($disabled == TRUE): ?> disabled<?php endif; ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>

                    <?php get_template_part('partials/header/dropdown-links'); ?>
                </div>

           <?php endif; ?>

        <?php endif; ?>

    <?php endwhile; endif; ?>

</div>