<?php if(get_field('show_demo_button', 'options') == TRUE): ?>
    <div class="demo-cta cta">
        <a href="#" class="blue-btn demo-trigger"><?php the_field('demo_button_label', 'options'); ?></a>
    </div>
<?php endif; ?>

<div class="contact-cta cta">
    <a href="#" class="blue-btn white-outline contact-trigger"><?php the_field('contact_form_headline', 'options'); ?></a>
</div>