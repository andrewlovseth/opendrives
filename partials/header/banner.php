<?php if(get_field('show_banner', 'options')): ?>

	<?php
		$banner = get_field('banner', 'options');
		$copy = $banner['copy'];
		$bg_image = $banner['background'];
		$bg_color = $banner['background_color'];
		$text_color = $banner['text_color'];
		$link_color = $banner['link_color'];
		$icon = $banner['icon'];
		$link = $banner['link'];


		$className = 'banner';
		if($bg_image) {
		    $className .= ' bg-image';
		}
		if($bg_color) {
		    $className .= ' bg-color';
		}
		if($icon) {
		    $className .= ' has-icon';
		}
	?>

	<style>
		aside.banner.bg-image {
			background-image: url(<?php echo $bg_image['url']; ?>);
		}

		aside.banner.bg-color {
			background-color: <?php echo $bg_color; ?>;
		}

		<?php if($text_color): ?>
			aside.banner p {
				color: <?php echo $text_color; ?>;
			}
		<?php endif; ?>

		<?php if($link_color): ?>
			aside.banner p strong {
				color: <?php echo $link_color; ?>;
			}
		<?php endif; ?>

	</style>

	<aside class="<?php echo $className; ?>">
		<div class="wrapper">

			<a class="link-wrapper" href="<?php echo $link; ?>" target="_blank">
				<?php if($icon): ?>
					<div class="icon">
						<img src="<?php echo $icon['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>				
				<?php endif; ?>

				<div class="copy">
					<?php echo $copy; ?>
				</div>
			</a>	

		</div>
	</aside>

<?php endif; ?>