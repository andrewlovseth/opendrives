<nav>
	<div class="overlay">
		<div class="overlay-wrapper">

			<div class="info">
				<div class="nav-links">

					<?php if(have_rows('navigation_links', 'options')): while(have_rows('navigation_links', 'options')): the_row(); ?>

					    <div class="link">
					    	<a class="blue-underline" href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
					    </div>

					<?php endwhile; endif; ?>
		    	
				</div>
			</div>
			
		</div>
	</div>
</nav>