<?php

    $features = get_field('features');
    $section_header = $features['headline'];

    if(have_rows('features')): while(have_rows('features')): the_row();

?>

    <section class="features">
        <div class="wrapper">

            <div class="section-header">
                <h2><?php echo $section_header; ?></h2>
            </div>

            <div class="features-grid">
                <?php if(have_rows('columns')): while(have_rows('columns')): the_row(); ?>
        
                    <div class="feature">
                        <div class="headline">
                            <h5><?php the_sub_field('headline'); ?></h5>
                        </div>

                        <div class="copy p4">
                            <?php the_sub_field('copy'); ?>
                        </div>                        

                        <div class="graphic">
                            <img src="<?php $image = get_sub_field('graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                        </div>
                    </div>
                <?php endwhile; endif; ?>
            </div>

        </div>
    </section>
    
<?php endwhile; endif; ?>