<?php

    $difference = get_field('difference');
    $photo = $difference['photo'];
    $headline = $difference['headline'];
    $copy = $difference['copy'];

?>

<section class="difference">
    <div class="wrapper">

        <div class="photo">
            <div class="content">
                <img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt']; ?>" />
            </div>
        </div>

        <div class="info">
            <div class="section-header">
                <h2><?php echo $headline; ?></h2>
            </div>

            <div class="copy p3 extended">
                <?php echo $copy; ?>
            </div>
        </div>                

    </div>
</section>