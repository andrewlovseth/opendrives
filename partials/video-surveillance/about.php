<?php

    $about = get_field('about');
    $section_header = $about['headline'];

    if(have_rows('about')): while(have_rows('about')): the_row();

?>

    <section class="about">
        <div class="wrapper">

            <div class="section-header">
                <h2><?php echo $section_header; ?></h2>
            </div>

            <div class="features">
                <?php if(have_rows('features')): while(have_rows('features')): the_row(); ?>
        
                    <div class="feature">
                        <?php the_sub_field('copy'); ?>
                    </div>

                <?php endwhile; endif; ?>
            </div>

        </div>
    </section>
    
<?php endwhile; endif; ?>