<section class="details">
    <div class="wrapper">

        <?php if(have_rows('details')): while(have_rows('details')): the_row(); ?>
            <div class="detail">
                <div class="photo">
                    <div class="content">
                        <img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </div>
                </div>

                <div class="info">
                    <div class="headline">
                        <h4><?php the_sub_field('headline'); ?></h4>
                    </div>

                    <div class="copy p3 extended">
                        <?php the_sub_field('copy'); ?>
                    </div>
                </div>                
            </div>
        <?php endwhile; endif; ?>

    </div>
</section>