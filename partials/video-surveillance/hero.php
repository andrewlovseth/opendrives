<?php

    $hero = get_field('hero');
    $icon = $hero['icon'];
    $headline = $hero['headline'];
    $copy = $hero['copy'];
    $photo = $hero['photo'];

?>

<section class="hero">
	<div class="content">

		<div class="photo">
			<img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt']; ?>" />
		</div>

		<div class="info">
			<div class="wrapper">
				<div class="headline">
                    <?php if($icon): ?>
                        <div class="icon">
                            <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />                        
                        </div>
                    <?php endif; ?>

					<h1><?php echo $headline; ?></h1>
				</div>

                <div class="copy p1">
                    <?php echo $copy; ?>
                </div>
			</div>
		</div>

	</div>
</section>