<section class="case-studies">
	<div class="wrapper">

		<div class="grid two-col">
			<?php if(have_rows('case_studies')): $count = 1; while(have_rows('case_studies')) : the_row(); ?>

				<?php if( get_row_layout() == 'case_study' ): ?>

					<div class="case-study">
						<div class="case-study-wrapper">

							<div class="logo">
								<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="info">
								<div class="copy p2">
									<?php the_sub_field('dek'); ?>
								</div>

								<div class="cta">
									<a class="blue-underline case-study-toggle" data-case-study="#case-study-<?php echo $count; ?>" href="#">Read More +</a>
								</div>


							 	<div id="case-study-<?php echo $count; ?>" class="full-case-study copy p4 extended">
							 		<?php the_sub_field('full_case_study'); ?>
							 	</div>
							</div>

						</div>
					</div>

				<?php endif; ?>

			<?php $count++; endwhile; endif; ?>		
		</div>
		


	</div>
</section>