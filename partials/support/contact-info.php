<section class="contact-info">
	<div class="wrapper">

		<div class="info">
			<div class="headline">
				<h4><?php the_field('support_contact_headline'); ?></h4>
			</div>

			<div class="copy p2">
				<p><?php the_field('support_contact_dek'); ?></p>
			</div>				
		</div>

		<div class="details">
			<div class="copy p3">
				<h4>Phone</h4>
				<p>
					<a href="tel:<?php the_field('support_phone'); ?>"><?php the_field('support_phone'); ?></a><br/>
				</p>

				<h4>Email</h4>
				<p><a href="mailto:<?php the_field('support_email'); ?>"><?php the_field('support_email'); ?></a></p>

				<h4>Address</h4>
				<p><?php the_field('support_address'); ?></p>					
			</div>
		</div>
		
	</div>
</section>