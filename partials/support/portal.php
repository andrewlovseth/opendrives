<section class="portal">
	<div class="wrapper">
		
		<div class="info">
			<div class="copy p2">
				<p><?php the_field('portal_dek'); ?></p>
			</div>

			<?php 
				$link = get_field('portal_link');
				if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			 ?>
			 
			 	<div class="cta">
			 		<a class="blue-btn black-outline" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
			 	</div>

			<?php endif; ?>
		</div>

	</div>
</section>