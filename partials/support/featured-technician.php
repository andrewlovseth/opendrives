<section class="featured-technician">
	<div class="wrapper">

		<div class="photo">
			<img src="<?php $image = get_field('featured_technician_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>

		<div class="info">
			<div class="headline">
				<h3><?php the_field('featured_technician_headline'); ?></h3>
			</div>

			<div class="copy p3 extended">
				<?php the_field('featured_technician_copy'); ?>
			</div>
		</div>

	</div>
</section>