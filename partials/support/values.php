<section class="values">
	<div class="wrapper">
		
		<div class="headline">
			<h2><?php the_field('values_headline'); ?></h2>
		</div>

		<div class="copy p2 extended">
			<?php the_field('values_dek'); ?>
		</div>

		<div class="grid three-col">
			<?php if(have_rows('values')): while(have_rows('values')): the_row(); ?>

			    <div class="item reason">
			    	<div class="photo">
			    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    	</div>

			    	<div class="headline">
			    		<h4><?php the_sub_field('headline'); ?></h4>
			    	</div>

			    	<div class="copy p4">
			    		<?php the_sub_field('dek'); ?>
			    	</div>				        
			    </div>

			<?php endwhile; endif; ?>
		</div>

	</div>
</section>