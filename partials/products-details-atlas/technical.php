<section class="technical">
	<div class="wrapper">
		
		<div class="copy p2">
			<?php the_field('technical_copy'); ?>
		</div>	

		<div class="graphic">
			<img src="<?php $image = get_field('technical_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>

	</div>
</section>