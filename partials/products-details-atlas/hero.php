<section class="hero-video">
	<div class="content">
		<div class="bg-video">
			<iframe style="background-color: #000;" id="background-video" src="https://player.vimeo.com/video/<?php the_field('hero_background_video'); ?>/?background=1&quality=1080p&transparent=0" frameborder="0" allow="autoplay"></iframe>
		</div>

		<div class="wrapper">

			<div class="info <?php echo sanitize_title_with_dashes(get_field('hero_product_name')); ?>">
				<div class="headline">
					<img src="<?php $image = get_field('hero_headline_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="sub-headline">
					<h3><?php the_field('hero_product_name'); ?></h3>
				</div>

				<div class="copy p1">
					<?php the_field('hero_dek'); ?>
				</div>
	
				<div class="image">
					<img src="<?php $image = get_field('hero_foreground_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			</div>	

		</div>		
	</div>
</section>