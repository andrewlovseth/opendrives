<section class="gallery">
	<div class="wrapper">
		
		<div class="headline">
			<h2><?php the_field('gallery_headline'); ?></h2>
		</div>

		<div class="gallery-wrapper">
			<?php
				$photosArray = array();
				$galleryImages = get_field('gallery');
				if( $galleryImages ): ?>

				<?php foreach( $galleryImages as $galleryImage ): ?>
					<?php array_push($photosArray, $galleryImage['id']); ?>
				<?php endforeach; ?>

			<?php endif; ?>

			<?php
				$photos = join(",", $photosArray);
				echo do_shortcode('[gallery columns="0" size="full" ids="' . $photos . '"]');
			?>				
		</div>
	
	</div>
</section>