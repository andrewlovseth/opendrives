<div class="cmo">
    <ul>
        <li class="positions">
            <h2 class="position-name">
                <a href="<?php echo site_url('/chief-marketing-officer/'); ?>">Chief Marketing Officer</a>
            </h2>
            <div class="position-meta">
                <span class="label fullTime"> Full-Time in </span>
                <i class="fa fa-map-marker"></i><span> Culver City, CA</span>
            </div>
        </li>
    </ul>
</div>