<section class="hero-video">
	<div class="content">
		<div class="bg-video">
			<iframe style="background-color: #000;" id="background-video" src="https://player.vimeo.com/video/<?php the_field('hero_background_video'); ?>/?background=1&quality=1080p&transparent=0" frameborder="0" allow="autoplay"></iframe>
		</div>

		<div class="wrapper">

			<div class="home-hero-info">
				<div class="headline">
					<img src="<?php $image = get_field('hero_headline'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="sub-headline">
					<h3><?php the_field('hero_sub_headline'); ?></h3>
				</div>

				<div class="copy p2">
					<?php the_field('hero_deck'); ?>
				</div>

				<div class="cta">
					<?php if(get_field('show_demo_button', 'options') == TRUE): ?>
						<a href="#" class="blue-btn demo-trigger"><?php the_field('hero_button_label'); ?></a>
					<?php endif; ?>

					<div class="image">
						<img src="<?php $image = get_field('hero_foreground_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				</div>	
			</div>
		</div>	

	</div>
</section>