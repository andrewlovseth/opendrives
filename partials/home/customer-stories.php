<section class="customer-stories">
	<div class="wrapper">

		<div class="section-header">
			<h2><?php the_field('customer_stories_header'); ?></h2>
		</div>
	
		<div class="customer-stories-carousel">
		
			<?php if(have_rows('customer_stories')): while(have_rows('customer_stories')): the_row(); ?>

				<?php $case_study = get_sub_field('case_study'); ?>
			    
			    <div class="case-study">
			    	<div class="photo">
			    		<a href="<?php echo get_permalink($case_study->ID); ?>">
			    			<img class="featured-photo" src="<?php $image = get_field('featured_image', $case_study->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    		</a>
			    	</div>

			    	<div class="info">
			    		<div class="logo">
				    		<img src="<?php $logo = get_field('logo', $case_study->ID); echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
			    		</div>

			    		<div class="headline">
			    			<h3><?php the_sub_field('headline'); ?></h3>
			    		</div>

			    		<div class="copy p3">
			    			<p><?php the_sub_field('dek'); ?></p>
			    		</div>

			    		<div class="cta">
			    			<a href="<?php echo get_permalink($case_study->ID); ?>" class="btn">View Customer Story</a>
			    		</div>
			    	</div>
			    </div>

			<?php endwhile; endif; ?>

		</div>	

	</div>
</section>