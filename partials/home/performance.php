<section class="performance">
	<div class="wrapper">
		
		<div class="section-header">
			<h2><?php the_field('performance_headline'); ?></h2>

			<div class="copy p2">
				<?php the_field('performance_deck'); ?>
			</div>
		</div>

		<div class="performance-tiles">
			
			<?php $count = 1; if(have_rows('performance_tiles')): while(have_rows('performance_tiles')) : the_row(); ?>

				<?php if( get_row_layout() == 'tile' ): ?>

					<?php 
						$link = get_sub_field('cta');
						if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					 ?>

						<div class="grid-item graphic-tile tile tile-<?php echo $count; ?>">
							<div class="content">

								<a href="<?php echo esc_url($link_url); ?>" class="tile-link" target="<?php echo esc_attr($link_target); ?>">
									<div class="graphic">
										<img src="<?php $image = get_sub_field('background'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</div>

									<div class="info">
										<div class="info-wrapper">
											<div class="headline">
												<h3><?php the_sub_field('headline'); ?></h3>
											</div>

											<div class="copy p4 deck">
												<?php the_sub_field('deck'); ?>
											</div>
										
											<div class="cta">
												<span class="underline"><?php echo esc_html($link_title); ?></span>
											</div>										
										</div>
	
									</div>
								</a>

							</div>
						</div>

					<?php endif; ?>					

				<?php endif; ?>

			<?php $count++ ;endwhile; endif; ?>

			<div class="grid-item tile tile-6 demo-tile">
				<div class="content">
	
					<a href="#" class="contact-trigger">
						<div class="info">
							<div class="info-wrapper">
								<div class="headline">
									<h3><?php the_field('performance_demo_headline'); ?></h3>
								</div>

								<div class="copy p4 deck">
									<?php the_field('performance_demo_deck'); ?>
								</div>
							
								<div class="cta">
									<span class="underline"><?php the_field('performance_demo_button_label'); ?></span>
								</div>							
							</div>
						</div>
					</a>

				</div>
			</div>

		</div>

	</div>
</section>