<section class="customers">
	<div class="wrapper">
		
		<div class="section-header">
			<h2><?php the_field('customers_headline'); ?></h2>

			<div class="copy p2">
				<p><?php the_field('customers_sub_headline'); ?></p>
			</div>
		</div>

		<div class="logos">					
			<?php $images = get_field('customers_logos'); if( $images ): ?>
				<?php foreach( $images as $image ): ?>

					<div class="logo">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
					
				<?php endforeach; ?>
			<?php endif; ?>
		</div>

	</div>
</section>