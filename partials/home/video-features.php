<section class="video-features">
	<div class="wrapper">

		<div class="section-header">
			<h2><?php the_field('video_features_headline'); ?></h2>

			<div class="copy p2">
				<?php the_field('video_features_deck'); ?>
			</div>
		</div>

		<?php if(have_rows('video_features')): ?>
			<div class="videos">

				<?php while(have_rows('video_features')) : the_row(); ?>
					<?php if( get_row_layout() == 'video' ): ?>

						<a href="#" class="video video-trigger" data-video-id="<?php the_sub_field('youtube_id'); ?>">
							<div class="content">
								<img src="<?php $image = get_sub_field('thumbnail'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							
								<div class="info">
									<div class="info-wrapper">
										<div class="headline">
											<h3><?php the_sub_field('headline'); ?></h3>
											<?php if(get_sub_field('optional_image')): ?>
												<div class="optional-graphic">
													<img src="<?php $image = get_sub_field('optional_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
												</div>
											<?php endif; ?>
										</div>

										<div class="meta">
											<div class="play-btn"></div>

											<div class="deck copy p3">
												<?php the_sub_field('deck'); ?>
											</div>										
										</div>
	
									</div>
								</div>									
							</div>
						</a>

					<?php endif; ?>
				<?php endwhile; ?>					

			</div>				
		<?php endif; ?>

	</div>
</section>