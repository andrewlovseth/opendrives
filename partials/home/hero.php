<?php if(have_rows('hero_sections')): while(have_rows('hero_sections')) : the_row(); ?>

	<?php if( get_row_layout() == 'background_video' ): ?>

		<?php
			$headline = get_sub_field('headline');
			$copy = get_sub_field('copy');
			$link = get_sub_field('link');
			$video = get_sub_field('video');

		?>

		<section id="<?php echo sanitize_title_with_dashes($headline); ?>" class="hero-video">
			<div class="content">
				<div class="bg-video">
					<iframe style="background-color: #000;" src="https://player.vimeo.com/video/<?php echo $video; ?>/?background=1&quality=1080p&transparent=0" frameborder="0" allow="autoplay"></iframe>
				</div>

				<div class="wrapper">

					<div class="home-hero-info">
						<div class="headline">
							<h1><?php echo $headline ?></h1>
						</div>

						<div class="copy p2">
							<?php echo $copy; ?>
						</div>

						<?php 
							if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>

							<div class="cta">
								<a class="blue-btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
							</div>

						<?php endif; ?>
					</div>		

				</div>		
			</div>
		</section>

	<?php endif; ?>

<?php endwhile; endif; ?>

