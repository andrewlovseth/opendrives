<section class="channel-partners">
	<div class="wrapper">

		<div class="section-header">
			<div class="headline">
				<h2><?php the_field('channel_partners_headline'); ?></h2>
			</div>

			<?php $channel_partners_copy = get_field('channel_partners_dek'); if($channel_partners_copy): ?>
				<div class="copy p2">
					<div class="col">
						<?php echo $channel_partners_copy['column_1']; ?>
					</div>

					<div class="col">
						<?php echo $channel_partners_copy['column_2']; ?>
					</div>
				</div>
			<?php endif; ?>				
		</div>

		<div class="featured partners-grid grid">
			<?php $images = get_field('featured_channel_partners'); if( $images ): ?>
				<?php foreach( $images as $image ): ?>

					<div class="partner">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	
		<div class="case-studies grid two-col">
			<?php if(have_rows('channel_partner_case_studies')): $count = 1; while(have_rows('channel_partner_case_studies')) : the_row(); ?>

				<?php if( get_row_layout() == 'case_study' ): ?>

					<div class="case-study">
						<div class="case-study-wrapper">
							<div class="logo">
								<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="info">
								<div class="copy p2">
									<?php the_sub_field('dek'); ?>
								</div>

								<div class="cta">
									<a class="blue-underline case-study-toggle" data-case-study="#case-study-<?php echo $count; ?>" href="#">Read More +</a>
								</div>


							 	<div id="case-study-<?php echo $count; ?>" class="full-case-study copy p4 extended">
							 		<?php the_sub_field('full_case_study'); ?>
							 	</div>
							</div>							
						</div>				
					</div>

				<?php endif; ?>

			<?php $count++; endwhile; endif; ?>		
		</div>
		

		<div class="additional partners-grid grid">
			<?php $images = get_field('additional_channel_partners'); if( $images ): ?>
				<?php foreach( $images as $image ): ?>

					<div class="partner">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
</section>