<section class="alliance-partners">
	<div class="wrapper">

		<div class="section-header">
			<div class="headline">
				<h2><?php the_field('alliance_partners_headline'); ?></h2>
			</div>

			<div class="copy p2">
				<?php the_field('alliance_partners_dek'); ?>
			</div>			
		</div>

		<div class="alliance partners-grid grid">
			<?php $images = get_field('alliance_partners'); if( $images ): ?>
				<?php foreach( $images as $image ): ?>

					<div class="partner">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	
	</div>
</section>