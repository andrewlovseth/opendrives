<section class="join">
	<div class="wrapper">
		
		<div class="headline">
			<h2><?php the_field('join_headline'); ?></h2>
		</div>

		<div class="copy p2">
			<?php the_field('join_dek'); ?>
		</div>

		<div class="grid three-col">
			<?php if(have_rows('join_features')): while(have_rows('join_features')): the_row(); ?>

			    <div class="item feature">
			    	<div class="photo">
			    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    	</div>

			    	<div class="copy p3">
			    		<?php the_sub_field('dek'); ?>
			    	</div>				        
			    </div>

			<?php endwhile; endif; ?>
		</div>

	</div>
</section>