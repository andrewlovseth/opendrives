<section class="leadership">
	<div class="wrapper">

		<div class="headline">
			<h2><?php the_field('leadership_headline'); ?></h2>
		</div>


		<?php if(have_rows('leadership_carousel')): $count = 1; while(have_rows('leadership_carousel')) : the_row(); ?>

			<?php if( get_row_layout() == 'carousel' ): ?>

				<div class="carousel carousel-<?php echo $count; ?>">
					<?php if(have_rows('carousel')): while(have_rows('carousel')): the_row(); ?>
					 
					    <div class="slide">
					    	<div class="info">
					    		<div class="info-wrapper">

						    		<div class="quote">
						    			<?php the_sub_field('quote'); ?>
						    		</div>

						    		<div class="source">
						    			<?php $source = get_sub_field('source'); if($source): ?>
							    			<h4>
							    				<span class="name"><?php echo $source['name']; ?>, <?php echo $source['position']; ?></span>

							    				<?php if($source['linkedin']): ?>
								    				<a href="<?php echo $source['linkedin']; ?>" class="linkedin" rel="external">

								    					<?php if($count == 2): ?>

									    					<img src="<?php bloginfo('template_directory'); ?>/images/icon-leadership-linkedin-black.svg" alt="LinkedIn" />

							    						<?php else: ?>

									    					<img src="<?php bloginfo('template_directory'); ?>/images/icon-leadership-linkedin-white.svg" alt="LinkedIn" />

									    				<?php endif; ?>

								    				</a>
								    			<?php endif; ?>
						    				</h4>
							    		<?php endif; ?>
						    		</div>
					    			
					    		</div>
					    	</div>

					    	<div class="photo">
					    		<div class="content">
						    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    		</div>
					    	</div>
					    </div>

					<?php endwhile; endif; ?>					
				</div>

			<?php endif; ?>

		<?php $count++; endwhile; endif; ?>


	</div>
</section>