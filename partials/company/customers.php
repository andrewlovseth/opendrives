<section class="customers">
	<div class="wrapper">
		
		<div class="headline">
			<h2><?php the_field('customers_headline'); ?></h2>
		</div>

		<?php if(have_rows('customers_grid')): ?>

			<div class="customers-grid">
				
				<?php $count = 1; while(have_rows('customers_grid')) : the_row(); ?>

					<?php if( get_row_layout() == 'text' ): ?>

						<div class="grid-item grid-item-<?php echo $count; ?> text">
							<div class="content">
								<div class="text-wrapper">
									<p><?php the_sub_field('copy'); ?></p>
								</div>	
							</div>						
						</div>

					<?php endif; ?>

					<?php if( get_row_layout() == 'image' ): ?>

						<div class="grid-item grid-item-<?php echo $count; ?> image">
							<div class="content">
								<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								
							</div>
						</div>

					<?php endif; ?>			

					<?php if( get_row_layout() == 'video' ): ?>

						<div class="grid-item grid-item-<?php echo $count; ?> video">
							<a href="#" class="video-trigger" data-video-id="<?php the_sub_field('youtube_id'); ?>">
								<div class="content">
									<img src="<?php $image = get_sub_field('thumbnail'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								</div>
							</a>

						</div>

					<?php endif; ?>		

				<?php $count++; endwhile; ?>

			</div>

		<?php endif; ?>

	</div>
</section>