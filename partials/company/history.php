<section class="history">
	<div class="wrapper">
		
		<div class="info">
			<div class="headline">
				<h2><?php the_field('history_headline'); ?></h2>
			</div>

			<div class="copy p4">
				<?php the_field('history_copy'); ?>
			</div>
		</div>

		<div class="photo">
			<img src="<?php $image = get_field('history_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>

	</div>
</section>