<?php if( get_row_layout() == 'hero' ): ?>

	<section class="hero">
		<div class="content">

			<div class="photo">
				<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />	
			</div>

			<div class="info">
				<div class="wrapper">
					<div class="headline">
						<h1>
							<?php if(get_sub_field('headline')): ?>
								<?php the_sub_field('headline'); ?>
							<?php else: ?>
								<?php the_title(); ?>
							<?php endif; ?>
						</h1>
					</div>

					<div class="copy p1">
						<p><?php the_sub_field('deck'); ?></p>
					</div>

					<?php $link = get_sub_field('cta'); if( $link ): ?>
						
						<?php
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>

						<div class="cta">
							<a href="<?php echo esc_url($link_url); ?>" class="btn"><?php echo esc_html($link_title); ?></a>
						</div>
					<?php endif; ?>

				</div>
			</div>

		</div>
	</section>

<?php endif; ?>