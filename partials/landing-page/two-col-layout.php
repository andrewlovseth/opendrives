<?php if( get_row_layout() == 'two_col_layout' ): ?>

	<section class="two-col-layout">
		<div class="wrapper">

			<?php if(have_rows('rows')): while(have_rows('rows')): the_row(); ?>
			 
			    <div class="row">
			    	<div class="photo">
			    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    	</div>

			    	<div class="info">
			    		<div class="headline">
			    			<h3><?php the_sub_field('headline'); ?></h3>
			    		</div>
		    		
		    			<div class="copy p2 extended">
		    				<?php the_sub_field('deck'); ?>
		    			</div>
			    	</div>
			        
			    </div>

			<?php endwhile; endif; ?>

		</div>
	</section>
	
<?php endif; ?>