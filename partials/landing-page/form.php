<?php if( get_row_layout() == 'form' ): ?>

	<section class="form"<?php if(get_sub_field('form_id')): ?>id="<?php the_sub_field('form_id'); ?>"<?php endif; ?>>
		<div class="wrapper">

			<div class="info">
				<?php if(get_sub_field('headline')): ?>
					<div class="headline">
						<h3><?php the_sub_field('headline'); ?></h3>
					</div>
				<?php endif; ?>

				<div class="copy p3">
					<p><?php the_sub_field('deck'); ?></p>
				</div>

				<div class="form-wrapper">
					<?php
						$shortcode = get_sub_field('form_shortcode');
						echo do_shortcode($shortcode);
					?>
				</div>
			</div>

		</div>
	</section>
	
<?php endif; ?>
