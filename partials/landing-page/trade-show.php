<?php if( get_row_layout() == 'trade_show' ): ?>

	<section class="trade-show">
		<div class="wrapper">

			<section class="trade-show-info">
				<div class="headline">
					<h1><?php the_sub_field('headline'); ?></h1>
				</div>

				<div class="copy deck p3">
					<?php the_sub_field('deck'); ?>
				</div>

				<div class="details">
					<div class="logo">
						<img src="<?php $image = get_sub_field('trade_show_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="copy p3">
						<?php the_sub_field('trade_show_details'); ?>
					</div>
				</div>				
			</section>

			<section class="form"<?php if(get_sub_field('form_id')): ?>id="<?php the_sub_field('form_id'); ?>"<?php endif; ?>>

				<div class="info">
					<?php if(get_sub_field('form_headline')): ?>
						<div class="headline">
							<h3><?php the_sub_field('form_headline'); ?></h3>
						</div>
					<?php endif; ?>

					<div class="copy p3">
						<p><?php the_sub_field('form_deck'); ?></p>
					</div>

					<div class="form-wrapper">
						<?php
							$shortcode = get_sub_field('form_shortcode');
							echo do_shortcode($shortcode);
						?>
					</div>
				</div>

			</section>
		

		</div>
	</section>


<?php endif; ?>
