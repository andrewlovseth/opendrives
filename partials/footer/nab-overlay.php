<?php

    $nab_overlay = get_field('nab_overlay', 'options');
    $link = $nab_overlay['link'];
    $graphic = $nab_overlay['graphic'];
    $show = $nab_overlay['show'];

    if($show == TRUE): 

?>

    <section id="nab-overlay">
        <div class="overlay">
            <div class="overlay-wrapper">

                <div class="info">
                    <div class="close">
                        <a href="#" class="nab-close-btn close-btn"></a>
                    </div>

                    <div class="nab-content">
                        <div class="content">
                            <a href="<?php echo esc_url($link); ?>" rel="external">
                                <?php echo wp_get_attachment_image($graphic['ID'], 'full'); ?>
                            </a>
                        </div>				    	
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    <script type="text/javascript">

        (function ($, window, document, undefined) {
            $(document).ready(function($) {

                // NAB OVERLAY
                var showModal = localStorage.getItem('showModal');

                if (sessionStorage.pageCount) {
                    sessionStorage.pageCount = Number(sessionStorage.pageCount) + 1;
                } else {
                    sessionStorage.pageCount = 1;
                }

                if(sessionStorage.pageCount == 1) {

                    if(showModal == null) {

                        localStorage.setItem('showModal', 1);
                        $('#nab-overlay').fadeIn('slow');

                    } else if(showModal >= 1 && showModal <= 2) {

                        var visit_count = parseInt(localStorage.getItem('showModal'));
                        visit_count++;
                        localStorage.setItem('showModal', visit_count);	        
                        $('#nab-overlay').fadeIn('slow');

                    } else {
                        
                        var visit_count = parseInt(localStorage.getItem('showModal'));
                        visit_count++;
                        localStorage.setItem('showModal', visit_count);	

                    }
                }

                // NAB Overlay Hide
                $('.nab-close-btn').click(function(){

                    $('#nab-overlay').fadeOut(200);

                    return false;
                });
            });
        })(jQuery, window, document);

    </script>

<?php endif; ?>