<?php if(get_field('show_demo_button', 'options') == TRUE): ?>

	<section class="demo-cta">
		<div class="wrapper">

			<div class="video-thumbnail">
				<a href="#" class="video-trigger" data-video-id="<?php the_field('demo_cta_video_id', 'options'); ?>">
					<img src="<?php $image = get_field('demo_cta_video_thumbnail', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="info-wrapper">
				<div class="info">
					<div class="headline">
						<h4><?php the_field('demo_cta_headline', 'options'); ?></h4>
					</div>

					<div class="copy p3">
						<p><?php the_field('demo_cta_copy', 'options'); ?></p>
					</div>
				</div>

				<div class="cta">
					<a href="#" class="demo-trigger btn white"><?php the_field('demo_button_label', 'options'); ?></a>
				</div>			
			</div>
					
		</div>
	</section>

<?php endif; ?>