<section class="solutions">
	<div class="wrapper">
		
		<div class="headline">
			<h2><?php the_field('solutions_headline'); ?></h2>
		</div>

		<div class="copy p1">
			<p><?php the_field('solutions_dek'); ?></p>
		</div>

		<div class="graphic">
			<img src="<?php $image = get_field('solutions_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>
		
		<?php $links_count = count(get_field('solutions_links')); if(have_rows('solutions_links')): ?>
			<div class="links link-<?php echo $links_count; ?>">		
				<?php while(have_rows('solutions_links')): the_row(); ?>
					<?php 
						$link = get_sub_field('link');
						if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					?>

						<div class="link cta">
							<a class="blue-btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						</div>

					<?php endif; ?>

				<?php endwhile; ?>			
			</div>			
		<?php endif; ?>

	</div>
</section>