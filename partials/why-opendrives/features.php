<section class="features">

	<?php if(have_rows('features')): while(have_rows('features')) : the_row(); ?>

		<?php if( get_row_layout() == 'section' ): ?>

			<section class="feature">
				<div class="wrapper">
					
					<div class="photo">
						<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="info">
			    		<div class="headline">
			    			<h4 class="sub-head"><?php the_sub_field('section_header'); ?></h4>
			    			<h3><?php the_sub_field('headline'); ?></h3>
			    		</div>

			    		<div class="copy p4">
			    			<p><?php the_sub_field('copy'); ?></p>
			    		</div>

			    		<div class="cta">
							<?php 

							$link = get_sub_field('link');

							if( $link ): 
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
								?>
								<a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
							<?php endif; ?>
			    		</div>
					</div>

				</div>
			</section>
						
	    <?php endif; ?>
	 
	<?php endwhile; endif; ?>
	
</section>