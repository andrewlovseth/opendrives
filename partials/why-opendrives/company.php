<section class="company">
	<div class="wrapper">
		
		<div class="logo">
			<img src="<?php $image = get_field('company_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>

		<div class="copy p1">
			<?php the_field('company_dek'); ?>
		</div>

	</div>
</section>