<section class="hero-video">
	<div class="content">
		<div class="bg-video">
			<iframe style="background-color: #000;" id="background-video" src="https://player.vimeo.com/video/<?php the_field('hero_background_video'); ?>/?background=1&quality=1080p&transparent=0" frameborder="0" allow="autoplay"></iframe>
		</div>

		
		<div class="wrapper">
			<div class="info">
				<div class="headline">
					<h1><?php the_field('hero_headline'); ?></h1>
				</div>

				<?php if(get_field('hero_dek')): ?>
					<div class="copy p1">
						<p><?php the_field('hero_dek'); ?></p>
					</div>
				<?php endif; ?>

				<?php if(get_field('hero_sub_headline')): ?>
					<div class="sub-headline">
						<h2><?php the_field('hero_sub_headline'); ?></h2>
					</div>
				<?php endif; ?>

			</div>	
		</div>

	</div>
</section>