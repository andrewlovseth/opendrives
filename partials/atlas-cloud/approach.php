<?php

    $approach = get_field('approach');
    $headline = $approach['headline'];
    $sub_headline = $approach['sub_headline'];
    $copy = $approach['copy'];
    $link = $approach['link'];
    $photo = $approach['photo'];

?>

<section class="approach">
    <div class="info">
        <div class="info-wrapper">

            <div class="sub-headline">
                <h4><?php echo $sub_headline; ?></h4>
            </div>

            <div class="headline">
                <h2><?php echo $headline; ?></h2>
            </div>

            <div class="copy p3 extended">
                <?php echo $copy; ?>
            </div>

            <?php 
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>

                <div class="cta">
                    <a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                </div>

            <?php endif; ?>


        </div>
    </div>

    <div class="photo">
        <div class="content">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </div>
    </div>

</section>