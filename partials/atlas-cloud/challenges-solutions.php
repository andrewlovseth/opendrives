<?php

    $challenges = get_field('challenges');
    $challenges_headline = $challenges['headline'];
    $challenges_copy = $challenges['copy'];

    $solutions = get_field('solutions');
    $solutions_headline = $solutions['headline'];
    $solutions_copy = $solutions['copy'];

?>

<section class="challenges-solutions">
    <div class="wrapper">

        <div class="challenges">
            <div class="headline">
                <h3><?php echo $challenges_headline; ?></h3>
            </div>

            <div class="copy p4 extended">
                <?php echo $challenges_copy; ?>
            </div>
        </div>

        <div class="solutions">
            <div class="headline">
                <h3><?php echo $solutions_headline; ?></h3>
            </div>

            <div class="copy p4 extended">
                <?php echo $solutions_copy; ?>
            </div>
        </div>

    </div>
</section>