<?php

    $acms = get_field('acms');
    $headline = $acms['headline'];
    $sub_headline = $acms['sub_headline'];
    $copy = $acms['copy'];
    $photo = $acms['photo'];

?>

<section class="acms">
    <div class="wrapper">

        <div class="section-header">
            <div class="headline">
                <h2><?php echo $headline; ?></h2>
            </div>

            <div class="sub-headline">
                <h4><?php echo $sub_headline; ?></h4>
            </div>
        </div>

        <div class="info">
            <div class="copy p3 extended">
                <?php echo $copy; ?>
            </div>
        </div>

        <div class="photo">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </div>

    </div>
</section>