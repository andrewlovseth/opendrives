<?php

    $architecture = get_field('architecture');
    $headline = $architecture['headline'];
    $photo = $architecture['photo'];
    $caption = $architecture['caption'];

?>

<section class="architecture">
    <div class="wrapper">

        <div class="headline">
            <h3><?php echo $headline; ?></h3>
        </div>

        <div class="photo">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </div>

        <div class="copy p3">
            <?php echo $caption; ?>
       </div>

    </div>
</section>