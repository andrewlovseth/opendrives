<?php
    $hero = get_field('hero');
    $headline = $hero['headline'];
    $sub_headline = $hero['sub_headline'];
    $deck = $hero['deck'];
    $link = $hero['link'];
    $photo = $hero['photo'];

?>

<section class="hero">
	<div class="content">

		<div class="photo">
			<img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt']; ?>" />
		</div>

		<div class="info">
			<div class="wrapper">
				<div class="headline">
					<h1><?php echo $headline; ?></h1>
				</div>

                <div class="sub-headline">
					<h2><?php echo $sub_headline; ?></h2>
				</div>

                <div class="copy p1">
                    <p><?php echo $deck; ?></p>
                </div>
			</div>
		</div>

	</div>
</section>