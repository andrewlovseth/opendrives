<?php

    $about = get_field('about');
    $headline = $about['headline'];
    $copy = $about['copy'];
    $photo = $about['photo'];

?>

<section class="about">
    <div class="wrapper">

        <div class="info">
            <div class="headline">
                <h2><?php echo $headline; ?></h2>
            </div>

            <div class="copy p3">
                <?php echo $copy; ?>
            </div>
        </div>

        <div class="photo">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </div>

    </div>
</section>