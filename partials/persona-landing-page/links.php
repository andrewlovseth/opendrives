<section class="links">
	<div class="wrapper">

		<div class="headline">
			<h2><?php the_field('links_headline'); ?></h2>
		</div>

		<?php if(have_rows('links')): ?>

			<div class="link-wrapper">
		
				<?php while(have_rows('links')) : the_row(); ?>

					<?php if( get_row_layout() == 'hyperlink' ): ?>

						<div class="link hyperlink">
							<div class="content">

								<div class="photo">
									<img src="<?php $image = get_sub_field('background_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />								
								</div>

								<div class="info">
									<div class="info-wrapper">
										<div class="headline">
											<h3><?php the_sub_field('headline'); ?></h3>
										</div>

										<?php 
											$link = get_sub_field('link');
											if( $link ): 
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self';
										 ?>
										 	<div class="cta">
										 		<a class="blue-btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
										 	</div>

										<?php endif; ?>
									</div>
								</div>

							</div>
						</div>

					<?php endif; ?>

					<?php if( get_row_layout() == 'video' ): ?>

						<div class="link video">
							<a href="#" class="video-trigger" data-video-id="<?php the_sub_field('youtube_id'); ?>">
								<div class="content">

									<div class="photo">
										<img src="<?php $image = get_sub_field('thumbnail'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</div>

									<div class="info">
										<div class="headline">
											<h3><?php the_sub_field('headline'); ?></h3>
										</div>
									</div>

								</div>
							</a>							
						</div>

					<?php endif; ?>

				<?php endwhile; ?>

			</div>

		<?php endif; ?>

	</div>
</section>