<section class="features">
	<div class="wrapper">
		
		<?php if(have_rows('features')): while(have_rows('features')): the_row(); ?>
		 
		    <div class="feature">
		    	<div class="photo">
		    		<div class="content">
		    			<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		    		</div>
		    	</div>

		    	<div class="info">
		    		<div class="info-wrapper">
		    			<p><?php the_sub_field('copy'); ?></p>
		    		</div>
		    	</div>
		    </div>

		<?php endwhile; endif; ?>

	</div>
</section>