<section class="message">
	<div class="wrapper">

		<div class="copy p1">
			<?php the_field('message_copy'); ?>
		</div>

	</div>
</section>