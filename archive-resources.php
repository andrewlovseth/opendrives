<?php get_header(); ?>

	<section class="hero">
		<div class="content">

			<div class="photo">
				<img src="<?php $image = get_field('resources_hero_image', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="info">
				<div class="wrapper">
					<div class="headline">
						<h1>
							<?php if(get_field('resources_hero_headline', 'options')): ?>
								<?php the_field('resources_hero_headline', 'options'); ?>
							<?php else: ?>
								<?php the_title(); ?>
							<?php endif; ?>
						</h1>
					</div>

					<div class="copy p1">
						<p><?php the_field('resources_hero_dek', 'options'); ?></p>
					</div>
				</div>
			</div>

		</div>
	</section>

	<section class="documents">

		<section class="document-list">
			<div class="wrapper">

				<div class="headline">
					<h2>White Papers</h2>
				</div>

				<?php echo do_shortcode('[ajax_load_more id="white-papers" scroll="false" container_type="div" theme_repeater="resource.php" post_type="resources" meta_key="type" meta_value="white-papers" button_label="More White Papers" posts_per_page="6"]'); ?>
				
			</div>
		</section>
		
		<section class="document-list">
			<div class="wrapper">

				<div class="headline">
					<h2>Videos</h2>
				</div>

				<?php echo do_shortcode('[ajax_load_more id="videos" scroll="false" container_type="div" theme_repeater="resource-video.php" post_type="resources" meta_key="type" meta_value="videos" button_label="More Videos" posts_per_page="6"]'); ?>
				
			</div>
		</section>
		
		<section class="document-list">
			<div class="wrapper">

				<div class="headline">
					<h2>Solution Briefs</h2>
				</div>

				<?php echo do_shortcode('[ajax_load_more id="solution-briefs" scroll="false" container_type="div" theme_repeater="resource.php" post_type="resources" meta_key="type" meta_value="solution-briefs" button_label="More Solution Briefs" posts_per_page="6"]'); ?>					
			</div>
		</section>
	
		<section class="document-list">
			<div class="wrapper">

				<div class="headline">
					<h2>Blog Posts</h2>
				</div>

				<?php echo do_shortcode('[ajax_load_more id="blog-posts" scroll="false" container_type="div" theme_repeater="resource-blog.php" post_type="post" category="resource" button_label="More Blog Posts" posts_per_page="6"]'); ?>
				
			</div>
		</section>

	</section>


<?php get_footer(); ?>

