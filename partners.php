<?php

/*

	Template Name: Partners

*/

get_header(); ?>

	<?php get_template_part('partials/content/hero'); ?>

	<?php get_template_part('partials/partners/portal'); ?>

	<?php get_template_part('partials/partners/channel-partners'); ?>

	<?php get_template_part('partials/partners/alliance-partners'); ?>

	<?php get_template_part('partials/partners/join'); ?>

	<?php get_template_part('partials/partners/contact'); ?>

<?php get_footer(); ?>