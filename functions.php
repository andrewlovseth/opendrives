<?php

/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/

function use_gd_editor($array) { 
    return array( 'WP_Image_Editor_GD', ); 
} 
add_filter( 'wp_image_editors', 'use_gd_editor' );



show_admin_bar( false );
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ));


add_theme_support( 'title-tag' );

function custom_tiled_gallery_width() {
    return '1200';
}
add_filter( 'tiled_gallery_content_width', 'custom_tiled_gallery_width' );


function add_file_types_to_uploads($file_types){
$new_filetypes = array();
$new_filetypes['svg'] = 'image/svg+xml';
$file_types = array_merge($file_types, $new_filetypes );
return $file_types;
}
add_filter('upload_mimes', 'add_file_types_to_uploads');

/*

    ----------------------------------------------------------------------
    					XX Code Cleanup
    ----------------------------------------------------------------------

*/


function remove_menus() {
    remove_menu_page( 'edit-comments.php' );          //Comments
}
add_action( 'admin_menu', 'remove_menus' );


/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/

// Register and noConflict jQuery 3.2.1
wp_register_script( 'jquery.3.2.1', 'https://code.jquery.com/jquery-3.2.1.min.js' );
wp_add_inline_script( 'jquery.3.2.1', 'var jQuery = $.noConflict(true);' );

// Enqueue custom styles and scripts
function enqueue_styles_and_scripts() {
    $uri = get_stylesheet_directory_uri();
    $dir = get_stylesheet_directory();

    $script_last_updated_at = filemtime($dir . '/js/site.js');
    $style_last_updated_at = filemtime($dir . '/style.css');

    // Add style.css
    wp_enqueue_style('adobe-fonts', 'https://use.typekit.net/lal4xcr.css');
    wp_enqueue_style('style', $uri . '/style.css' , '', $style_last_updated_at);

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script('custom-plugins', $uri . '/js/plugins.js', array( 'jquery.3.2.1' ), $script_last_updated_at, true);
    wp_enqueue_script('custom-site', $uri . '/js/site.js', array( 'jquery.3.2.1' ), $script_last_updated_at, true);

}
add_action( 'wp_enqueue_scripts', 'enqueue_styles_and_scripts', 0 );




/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/

function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}


if( function_exists('acf_add_options_page') ) {
    
    // add parent
    $parent = acf_add_options_page(array(
        'page_title'    => 'Site Options',
        'menu_title'    => 'Site Options',
        'redirect'      => false
    ));
    
    
    // add sub page
    acf_add_options_sub_page(array(
        'page_title'    => 'Customer Stories',
        'menu_title'    => 'Customer Stories',
        'parent_slug'   => $parent['menu_slug'],
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Resources',
        'menu_title'    => 'Resources',
        'parent_slug'   => $parent['menu_slug'],
    ));
    
}

// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');