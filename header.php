<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<?php wp_head(); ?>

	<?php the_field('js_head', 'options'); ?>
</head>

<body <?php body_class(); ?>>
	<?php the_field('js_body_top', 'options'); ?>

	<?php get_template_part('partials/header/banner'); ?>

	<?php get_template_part('partials/header/nab-banner'); ?>

	<header class="site-header">
		<div class="wrapper">
			<?php get_template_part('partials/header/logo'); ?>

			<?php get_template_part('partials/header/main-links'); ?>

			<?php get_template_part('partials/header/demo-btn'); ?>

			<?php get_template_part('partials/header/hamburger'); ?>
		</div>
	</header>

	<?php get_template_part('partials/header/navigation'); ?>