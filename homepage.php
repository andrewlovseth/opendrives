<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<?php get_template_part('partials/home/hero'); ?>

	<?php get_template_part('partials/home/video-features'); ?>

	<?php get_template_part('partials/home/performance'); ?>

	<?php get_template_part('partials/home/customer-stories'); ?>

	<?php get_template_part('partials/home/customers'); ?>

<?php get_footer(); ?>