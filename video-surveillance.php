<?php

/*

	Template Name: Video Surveillance

*/

get_header(); ?>

    <?php get_template_part('partials/video-surveillance/hero'); ?>

    <?php get_template_part('partials/video-surveillance/about'); ?>

    <?php get_template_part('partials/video-surveillance/details'); ?>

    <?php get_template_part('partials/video-surveillance/features'); ?>

    <?php get_template_part('partials/video-surveillance/difference'); ?>

    <?php get_template_part('partials/video-surveillance/contact'); ?>

<?php get_footer(); ?>