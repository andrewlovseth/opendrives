	<footer class="site-footer">
		<div class="wrapper">

			<section class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</section>

			<section class="columns">
				<div class="col navigation">
					<div class="col-header">
						<h4>Navigation</h4>
					</div>

					<div class="links">
						<?php if(have_rows('footer_links', 'options')): while(have_rows('footer_links', 'options')): the_row(); ?>
 
						    <div class="link">
						    	<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
						    </div>

						<?php endwhile; endif; ?>
					</div>
				</div>
	
				<div class="col contact">
					<div class="col-header">
						<h4>Contact Info</h4>
					</div>

					<div class="info">
						<p><?php the_field('address', 'options'); ?></p>

						<p>
							<a href="tel:<?php the_field('phone', 'options'); ?>"><?php the_field('phone', 'options'); ?></a><br/>
							<a href="tel:<?php the_field('toll_free_phone', 'options'); ?>"><?php the_field('toll_free_phone', 'options'); ?></a>
						</p>

						<p>
						<?php if(have_rows('emails', 'options')): while(have_rows('emails', 'options')): the_row(); ?>
							<a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a><br/>
						<?php endwhile; endif; ?>
						</p>
					</div>
				</div>

				<div class="col social">
					<div class="col-header">
						<h4>Social</h4>
					</div>

					<div class="social-links">

						<div class="link">
							<a href="<?php the_field('linkedin', 'options'); ?>" class="linkedin" rel="external">
								<img src="<?php bloginfo('template_directory') ?>/images/linkedin-icon-white.svg" alt="LinkedIn">
							</a>
						</div>

						<div class="link">
							<a href="<?php the_field('youtube', 'options'); ?>" class="youtube" rel="external">
								<img src="<?php bloginfo('template_directory') ?>/images/youtube-icon-white.svg" alt="YouTube">
							</a>
						</div>	

						<div class="link">
							<a href="<?php the_field('facebook', 'options'); ?>" class="facebook" rel="external">
								<img src="<?php bloginfo('template_directory') ?>/images/facebook-icon-white.svg" alt="Facebook">
							</a>
						</div>
				
					</div>

					<?php $badges = get_field('badges', 'options'); if( $badges ): ?>
						<div class="badges">
							<?php foreach( $badges as $badge): ?>
								<div class="badge">
									<?php echo wp_get_attachment_image($badge['ID'], 'full'); ?>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>

			</section>

			<section class="copyright">
				<p><?php the_field('copyright', 'options'); ?></p>				
			</section>

		</div>
	</footer>

	<?php get_template_part('partials/content/video-overlay'); ?>

	<?php get_template_part('partials/content/contact-overlay'); ?>

	<?php get_template_part('partials/content/demo-overlay'); ?>

	<?php wp_footer(); ?>

	<?php the_field('js_body_bottom', 'options'); ?>

	<?php get_template_part('partials/footer/nab-overlay'); ?>

</body>
</html>