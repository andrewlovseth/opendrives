<?php

/*

	Template Name: Products Details - Atlas

*/

get_header(); ?>

	<?php get_template_part('partials/products-details/hero'); ?>

	<?php get_template_part('partials/products-details-atlas/product-description'); ?>

	<?php get_template_part('partials/products-details-atlas/gallery'); ?>

	<?php get_template_part('partials/products-details-atlas/technical'); ?>

<?php get_footer(); ?>