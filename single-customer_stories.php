<?php get_header(); ?>

	<section class="page-header">
		<div class="wrapper">
			
			<div class="headline">
				<h4><a href="<?php echo site_url('/customer-stories/'); ?>">Customer Stories</a></h4>
				<h1><?php the_title(); ?></h1>
			</div>

		</div>
	</section>

	<section class="featured-media">
		<div class="wrapper">
			
			<?php if(get_field('video_id')): ?>
				
				<div class="video">
					<iframe width="1920" height="1080" allowfullscreen frameborder="0" allowTransparency="true" src="https://www.youtube.com/embed/<?php the_field('video_id'); ?>?autoplay=1& modestbranding=1&rel=0"></iframe>
				</div>

			<?php else: ?>

				<div class="photo">
					<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			<?php endif; ?>

		</div>
	</section>

	<section class="content">
		<div class="wrapper">
			
			<article class="copy p2 extended">
				<?php the_field('content'); ?>
			</article>	

			<?php if(get_field('sidebar')): ?>
				<aside class="copy p3 extended">
					<?php the_field('sidebar'); ?>
				</aside>
			<?php endif; ?>

		</div>
	</section>

<?php get_footer(); ?>