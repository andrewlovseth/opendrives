<?php

/*

	Template Name: Job Listing

*/

get_header(); ?>

	<section class="generic">
		<div class="wrapper">

			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

				<article>
					<div class="page-title">
						<h1><?php the_title(); ?></h1>			
					</div>

					<div class="copy p3 extended">
						<?php the_content(); ?>
					</div>
						
				</article>

			<?php endwhile; endif; ?>

		</div>
	</section>

<?php get_footer(); ?>