<?php

/*

	Template Name: Landing Page

*/

$page_id = get_field('page_id');
$background_image = get_field('background_image');
$background_color = get_field('background_color');
$text_color = get_field('text_color');

get_header(); ?>

<?php if(have_rows('sections')): ?>

	<section class="landing-page"<?php if($page_id): ?> id="<?php echo $page_id; ?>"<?php endif; ?>>

		<?php while(have_rows('sections')) : the_row(); ?>

			<?php get_template_part('partials/landing-page/hero'); ?>

			<?php get_template_part('partials/landing-page/form'); ?>

			<?php get_template_part('partials/landing-page/two-col-layout'); ?>	

			<?php get_template_part('partials/landing-page/trade-show'); ?>	 

		<?php endwhile; ?>

	</section>

<?php endif; ?>


<?php if($background_image): ?>
	<style>
		#<?php echo $page_id; ?> {
			background-color: <?php echo $background_color; ?>;
			background-image: url(<?php echo $background_image['url']; ?>);
			background-repeat: no-repeat;
			background-position: center top;
			background-size: cover;
			color: <?php echo $text_color; ?>;
		}

	</style>
<?php endif; ?>

<?php get_footer(); ?>