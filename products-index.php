<?php

/*

	Template Name: Products Index

*/

get_header(); ?>

	<?php get_template_part('partials/content/hero'); ?>

	<?php get_template_part('partials/products-index/atlas'); ?>

	<?php get_template_part('partials/products-index/new-products'); ?>

	<?php get_template_part('partials/products-index/legacy-products'); ?>

<?php get_footer(); ?>