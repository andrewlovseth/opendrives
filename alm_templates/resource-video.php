<?php

    $video = get_field('video');
    $image = $video['thumbnail'];
    $caption = $video['caption'];
    $source = $video['source'];
    $youtube_id = $video['youtube_id'];
    $vimeo_id = $video['vimeo_id'];

    if($source == 'youtube') {
        $video_id = $youtube_id;
        $class = "video-trigger";
    } elseif($source == 'vimeo') {
        $video_id = $vimeo_id;
        $class = "vimeo-video-trigger";
    }

    

?>

<div class="resource resource-video">
    <div class="thumbnail">
        <a href="#" class="<?php echo $class; ?>" data-video-id="<?php echo $video_id; ?>">
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        </a>
    </div>

    <div class="caption">
        <div class="copy p4">
            <p><?php echo $caption; ?></p>
        </div>
    </div>                
</div>