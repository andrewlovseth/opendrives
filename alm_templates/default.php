<article>
	<?php if(get_field('featured_image')): ?>
		<div class="photo">
			<a href="<?php the_permalink(); ?>">
	        	<div class="content">
					<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
	        	</div>
			</a>
		</div>
	<?php else: ?>
		<div class="photo">
			<a href="<?php the_permalink(); ?>">
	        	<div class="content">
	        		<img src="<?php bloginfo('template_directory'); ?>/images/fallback-news-featured-image.jpg" alt="<?php the_title(); ?>" />
	        	</div>
			</a>
		</div>
	<?php endif; ?>

	<div class="info">
		<div class="headline">
			<span class="time"><?php the_time('j M Y'); ?></span>
			<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
		</div>

		<?php if(get_field('teaser')): ?>
			<div class="copy p4">
				<?php $teaser = wp_trim_words(get_field('teaser'), $num_words = 20, $more = '...' ); echo $teaser; ?>
			</div>

		<?php else: ?>
			<div class="copy p4">
				<?php $teaser = wp_trim_words(get_the_content(), $num_words = 20, $more = '...' ); echo $teaser; ?>
			</div>
		<?php endif; ?>

	</div>	
</article>