<div class="resource resource-blog">
	<div class="photo">
		<div class="content">
			<a href="<?php the_permalink(); ?>">
				<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</div>
	</div>

	<div class="info">
		<div class="headline">
			<span class="time"><?php the_time('j M Y'); ?></span>
			<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
		</div>

		<div class="copy p4">
			<?php the_field('teaser'); ?>
		</div>
	</div>

</div>