<div class="resource resource-file">
   	<?php $file = get_field('file'); ?>
	<a href="<?php echo $file['url']; ?>" target="_blank">
    	<div class="info">
    		<div class="headline">
      		  	<h5><?php the_title(); ?></h5>
    		</div>

		    <div class="copy p4">
    		    <p><?php the_field('description'); ?></p>
    		</div>
    	</div>
    </a>
</div>