<?php

/*

	Template Name: Products Details

*/

get_header(); ?>


	<?php get_template_part('partials/products-details/hero'); ?>

	<?php get_template_part('partials/products-details/description'); ?>

	<?php get_template_part('partials/products-details/features'); ?>

	<?php get_template_part('partials/products-details/options'); ?>

	<?php get_template_part('partials/products-details/tech-specs'); ?>

<?php get_footer(); ?>