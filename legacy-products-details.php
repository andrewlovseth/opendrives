<?php

/*

	Template Name: Legacy Products Details

*/

get_header(); ?>

	<section class="page-header">
		<div class="wrapper">
			
			<div class="headline">
				<h1><?php the_title(); ?></h1>
			</div>

			<div class="copy p2">
				<p><?php the_field('tagline'); ?></p>
			</div>

		</div>
	</section>

	<section class="product-details">
		<div class="wrapper">
			
			<div class="product-teaser">
				<div class="photo">
					<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />			
				</div>
			</div>

			<div class="info">
				<div class="copy p4 extended">
					<?php the_field('product_description'); ?>
				</div>

				<div class="cta">
					<a href="<?php the_field('product_sheet'); ?>" class="btn large" rel="external">Read our product overview</a>
				</div>
			</div>


		</div>
	</section>

	<section class="product-details">

		<?php if(get_field('uses_copy')): ?>
			<section class="uses">
				<div class="container">

					<div class="headline">
						<h3 class="product-header"><?php the_field('uses_headline'); ?></h3>
					</div>

					<div class="copy p4">
						<?php the_field('uses_copy'); ?>
					</div>
					

				</div>
			</section>
		<?php endif; ?>

		<?php if(get_field('features_copy')): ?>
			<section class="features">
				<div class="container">

					<div class="headline">
						<h3 class="product-header"><?php the_field('features_headline'); ?></h3>
					</div>

					<div class="copy p4">
						<?php the_field('features_copy'); ?>
					</div>					

				</div>
			</section>
		<?php endif; ?>
		
	</section>




	<?php $posts = get_field('customer_stories'); if( $posts ): ?>
		<section class="customer-stories">
			<div class="wrapper">

				<div class="headline section-header">
					<h2>Customer Stories</h2>
				</div>

				<div class="customer-stories-grid">
					<?php foreach( $posts as $p ): ?>

						<div class="customer-story">
							<div class="photo">
								<a href="<?php echo get_permalink( $p->ID ); ?>">
									<img src="<?php $image = get_field('featured_image', $p->ID); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
								</a>
							</div>

							<div class="info">
								<div class="headline">
									<h3><a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a></h3>
								</div>

								<div class="copy p4">
									<?php the_field('teaser_description', $p->ID); ?>
								</div>
							</div>
						</div>

					<?php endforeach; ?>
				</div>				

			</div>
		</section>
	<?php endif; ?>


<?php get_footer(); ?>