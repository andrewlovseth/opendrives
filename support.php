<?php

/*

	Template Name: Support

*/

get_header(); ?>

	<?php get_template_part('partials/support/portal'); ?>

	<?php get_template_part('partials/content/hero'); ?>

	<?php get_template_part('partials/support/values'); ?>

	<?php get_template_part('partials/support/case-studies'); ?>

	<?php get_template_part('partials/support/featured-technician'); ?>

	<?php get_template_part('partials/support/contact-info'); ?>

<?php get_footer(); ?>