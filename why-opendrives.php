<?php

/*

	Template Name: Why OpenDrives

*/

get_header(); ?>

	<?php get_template_part('partials/why-opendrives/hero'); ?>

	<?php get_template_part('partials/why-opendrives/solutions'); ?>

	<?php get_template_part('partials/why-opendrives/features'); ?>

	<?php get_template_part('partials/why-opendrives/company'); ?>

<?php get_footer(); ?>