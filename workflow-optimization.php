<?php

/*

	Template Name: Workflow Optimization

*/

get_header(); ?>

	<?php get_template_part('partials/content/hero'); ?>


	<section class="feature-grid" id="uses">
		<div class="wrapper">

			<div class="section-header">
				<div class="headline">
					<h2><?php the_field('uses_headline'); ?></h2>
				</div>

				<div class="copy p2">
					<p><?php the_field('uses_dek'); ?></p>
				</div>
			</div>

			<div class="grid">

				<?php if(have_rows('uses')): while(have_rows('uses')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'use' ): ?>
						
						<div class="item">
							<div class="photo">
								<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="info">
								<div class="headline">
									<h3><?php the_sub_field('headline'); ?></h3>
								</div>

								<div class="copy p4">
									<p><?php the_sub_field('dek'); ?></p>
								</div>

								<?php $link = get_sub_field('cta'); if( $link ): ?>
									
									<?php
										$link_url = $link['url'];
										$link_title = $link['title'];
										$link_target = $link['target'] ? $link['target'] : '_self';
									?>

									<div class="cta">
										<a href="<?php echo esc_url($link_url); ?>" class="btn" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
									</div>
								<?php endif; ?>
							</div>
				    		
						</div>
						
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>				

			</div>
			
		</div>
	</section>


	
	<section class="feature-grid" id="integrations">
		<div class="wrapper">

			<div class="section-header">
				<div class="headline">
					<h2><?php the_field('integrations_headline'); ?></h2>
				</div>

				<div class="copy p2">
					<p><?php the_field('integrations_dek'); ?></p>
				</div>
			</div>

			<div class="grid">

				<?php if(have_rows('integrations')): while(have_rows('integrations')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'integration' ): ?>
						
						<div class="item">
							<div class="photo">
								<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="info">
								<div class="headline">
									<h3><?php the_sub_field('headline'); ?></h3>
								</div>

								<div class="copy p4">
									<p><?php the_sub_field('dek'); ?></p>
								</div>

								<?php $link = get_sub_field('cta'); if( $link ): ?>
									
									<?php
										$link_url = $link['url'];
										$link_title = $link['title'];
										$link_target = $link['target'] ? $link['target'] : '_self';
									?>

									<div class="cta">
										<a href="<?php echo esc_url($link_url); ?>" class="btn" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
									</div>
								<?php endif; ?>
							</div>
				    		
						</div>
						
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>				

			</div>
			
		</div>
	</section>




<?php get_footer(); ?>