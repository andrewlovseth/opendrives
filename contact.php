<?php

/*

	Template Name: Contact

*/

get_header(); ?>

	<section class="page-header">
		<div class="wrapper">
			
			<div class="headline">
				<h2><?php the_field('headline'); ?></h2>
			</div>

			<div class="copy p2">
				<?php the_field('dek'); ?>
			</div>

		</div>
	</section>

	<section class="contact-info">
		<div class="wrapper">

			<div class="info copy p2">
				<h4>Address</h4>
				<p><?php the_field('address', 'options'); ?></p>

				<h4>Phone</h4>
				<p>
					<a href="tel:<?php the_field('phone', 'options'); ?>"><?php the_field('phone', 'options'); ?></a><br/>
					<a href="tel:<?php the_field('toll_free_phone', 'options'); ?>"><?php the_field('toll_free_phone', 'options'); ?></a>
				</p>

				<h4>Email</h4>
				<p>
					<?php if(have_rows('emails', 'options')): while(have_rows('emails', 'options')): the_row(); ?>
						<a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a><br/>
					<?php endwhile; endif; ?>
				</p>
			</div>

			<div class="photo">
				<img src="<?php $image = get_field('office_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />				
			</div>
			
		</div>
	</section>

	<section class="map">
		<div class="wrapper">
			
			<div class="map-view-wrapper">
				<div class="content">
					<?php the_field('map_embed'); ?>
				</div>
			</div>

		</div>
	</section>

<?php get_footer(); ?>