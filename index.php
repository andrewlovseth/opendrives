<?php

$page_for_posts = get_option( 'page_for_posts' );

get_header(); ?>

	<section class="hero">
		<div class="content">

			<div class="photo">
				<img src="<?php $image = get_field('news_hero_image', $page_for_posts ); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="info">
				<div class="wrapper">
					<div class="headline">
						<h1><?php the_field('news_headline', $page_for_posts ); ?></h1>
					</div>

					<div class="copy p1">
						<p><?php the_field('news_dek', $page_for_posts ); ?></p>
					</div>
				</div>
			</div>

		</div>
	</section>


	<section class="recent">
		<div class="wrapper">
			
			<article class="featured-post">
				<?php
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 1,
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<div class="photo">
						<a href="<?php the_permalink(); ?>">
							<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</a>						
					</div>

					<div class="info">
						<div class="headline">
							<span class="time"><?php the_time('j M Y'); ?></span>
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						</div>

						<div class="copy p2">
							<?php the_field('teaser'); ?>
						</div>

						<div class="cta">
							<a href="<?php the_permalink(); ?>" class="btn">Read More</a>
						</div>
					</div>

				<?php endwhile; endif; wp_reset_postdata(); ?>
			</article>

			<div class="recent-posts">

				<div class="articles-wrapper">

					<?php
						$args = array(
							'post_type' => 'post',
							'posts_per_page' => 3,
							'offset' => 1,
						);
						$query = new WP_Query( $args );
						if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

						<article>
							<div class="info">
								<div class="headline">
									<span class="time"><?php the_time('j M Y'); ?></span>
									<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
								</div>
							</div>

							<div class="copy p4">
								<?php $excerpt = wp_trim_words( get_field('teaser' ), $num_words = 20, $more = '...' ); echo $excerpt; ?>
							</div>
						</article>

					<?php endwhile; endif; wp_reset_postdata(); ?>
					
				</div>

		</div>
	</section>


	<section class="all-posts">
		<div class="wrapper">

			<div class="section-header headline">
				<h2>All Posts</h2>
			</div>

			<?php echo do_shortcode('[ajax_load_more container_type="div" theme_repeater="default.php" css_classes="grid three-col" post_type="post" posts_per_page="12" post__not_in="199,205" scroll="false" transition_container="false" button_label="Load More Posts"]'); ?>
			
		</div>
	</section>




<?php get_footer(); ?>