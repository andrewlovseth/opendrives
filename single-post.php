<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<article>
			<div class="wrapper">

				<section class="page-header article-header">
						
					<div class="headline">
						<h4 class="sub-head"><span class="time"><?php the_time('j M Y'); ?></span></h4>
						<h1><?php the_title(); ?></h1>
					</div>

				</section>

				<section class="article-body">

					<div class="copy p2 extended">
						<?php the_content(); ?>
					</div>
					
				</section>

				<section class="article-footer">
					
				</section>
				

			</div>
		</article>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>