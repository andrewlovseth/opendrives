<?php

/*

	Template Name: EBR

*/

get_header(); ?>

    <?php get_template_part('partials/ebr/hero'); ?>

    <?php get_template_part('partials/ebr/solutions-header'); ?>

    <?php get_template_part('partials/ebr/comparison'); ?>

    <?php get_template_part('partials/ebr/diagram'); ?>

    <?php get_template_part('partials/ebr/difference'); ?>

    <?php get_template_part('partials/ebr/contact'); ?>

<?php get_footer(); ?>