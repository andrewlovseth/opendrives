<?php

/*

	Template Name: Company

*/

get_header(); ?>

	<?php get_template_part('partials/content/hero'); ?>

	<?php get_template_part('partials/company/customers'); ?>

	<?php get_template_part('partials/company/history'); ?>

	<?php get_template_part('partials/company/leadership'); ?>

	<?php get_template_part('partials/company/contact'); ?>

<?php get_footer(); ?>