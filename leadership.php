<?php

/*

	Template Name: Leadership

*/

get_header(); ?>

	<?php get_template_part('partials/leadership/page-header'); ?>

    <?php get_template_part('partials/leadership/team'); ?>

    <?php get_template_part('partials/leadership/cta'); ?>

<?php get_footer(); ?>